Claw:
  openLeftClaw
  closeLeftClaw
  openRightClaw
  closeRightClaw
  defaultLeft
  defaultRight
  openNoodle
  closeNoodle

Elevator:
    elevatorRaw <value> <time>
    zero
    elevatorSet <value>

Drive:
    drive <leftPower> <rightPower> <time>
