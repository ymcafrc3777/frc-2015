package io.ibj.lima3777.rr2015;

/**
 * @author joe 1/15/2015
 */
public enum BotState {
    DISABLED,
    TELEOP,
    AUTO,
    TEST
}
