package io.ibj.lima3777.rr2015;

import io.ibj.lima3777.rr2015.driveController.DriveController;
import io.ibj.lima3777.rr2015.module.Module;
import io.ibj.lima3777.rr2015.script.Scriptable;

/**
 * @author joe
 *         1/11/2015
 */
public interface DriveBase extends Module, Scriptable {
    
    DriveController getCurrentController();
    void setDriveController(DriveController controller);
    
    double getX();
    double getY();
    double getHeading();
    
    void setX(double x);
    void setY(double y);
    void setHeading(double heading);
    
    DriveController getFallbackController();
    void setFallbackController(DriveController controller);
}
