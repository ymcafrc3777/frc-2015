package io.ibj.lima3777.rr2015;

import edu.wpi.first.wpilibj.Joystick;

/**
 * @author joe 1/15/2015
 * Removes deadband from joystick.
 */
public class BeanJoystick extends Joystick {
    public BeanJoystick(int port) {
        super(port);
    }

    @Override
    public double getRawAxis(int axis) {
        double rawValue = super.getRawAxis(axis);
        if(rawValue < 0.05 && rawValue > -0.05){
            return 0;
        }
        if (rawValue > 0){
        	return Math.pow(2.03619, rawValue)- 1.03619;
        }
        if (rawValue < 0){
        	return -1*(Math.pow(2.03619, -1*rawValue)- 1.03619);
        }
        return rawValue;
    }


}
