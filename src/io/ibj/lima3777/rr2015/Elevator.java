package io.ibj.lima3777.rr2015;

import io.ibj.lima3777.rr2015.module.Module;
import io.ibj.lima3777.rr2015.script.Scriptable;

/**
 * @author joe
 * 
 * Represents the elevator portion of the robot. The robot can use the elevator in one of two ways: Targeted mode, or raw mode. Targeted mode is used
 * by setting a target through setElevatorTarget(value), where value is a value 0->1, 0 being lowest and 1 being the highest. Once the elevator reaches
 * the position, the elevator will halt.
 *
 *
 */
public interface Elevator extends Module,Scriptable{
    /**
     * Sets a new elevator target. If out of range, will throw an IllegalArgumentException. If targeting is not enabled, will throw an IllegalStateException.
     * @param target New target of the elevator, 0->1
     */
    void setElevatorTarget(double target);

    /**
     * Resets the elevator encoder so that the new elevator position is 0. This should only be used internally, or if you know what you are doing.
     */
    void resetElevatorEncoder();

    /**
     * Returns the current elevator target. If targeting is disabled, getElevatorPosition() will be returned.
     * @return Current elevator target, 0->1. If targeting is not enabled, getElevatorPosition() is returned.
     */
    double getElevatorTarget();

    /**
     * Returns the current elevator position. 
     * @return Current elevator position.
     */
    double getElevatorPosition();

    /**
     * Returns whether targeting is enabled. 
     * @return If targeting is enabled.
     */
    boolean isTargetEnabled();

    /**
     * Sets if targeting is enabled. This will stop targeting and halt the motor if disabled in the middle of targeting.
     * @param enable Whether targeting should be enabled.
     */
    void enableTargeting(boolean enable);

    /**
     * Sets the raw motor speed. If targeting is enabled, this will be reset every clock iteration. Value should be within -1 -> 1, where all negative values 
     * will set the motor in reverse. If the value is out of range, this function will throw an IllegalArgumentException. 
     * @param value New motor speed. 
     */
    void setRawMotor(double value);

    /**
     * Returns the current elevator motor speed.
     * @return Current motor speed
     */
    public double getRawMotor();

    /**
     * Forces the elevator to rezero itself. 
     */
    void forceReZero();

    /**
     * Returns whether the elevator has zeroed itself yet. If false, set methods will throw an IllegalStateException();
     * @return Whether the elevator is zeroed.
     */
    boolean isZeroed();
}
