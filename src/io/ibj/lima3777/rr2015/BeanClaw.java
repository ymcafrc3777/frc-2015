package io.ibj.lima3777.rr2015;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.Solenoid;
import io.ibj.lima3777.rr2015.log.Log;
import io.ibj.lima3777.rr2015.module.ModuleMeta;
import io.ibj.lima3777.rr2015.script.FallbackFutureTask;
import io.ibj.lima3777.rr2015.script.ScriptElement;
import io.ibj.lima3777.rr2015.script.ScriptException;

import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import static edu.wpi.first.wpilibj.DoubleSolenoid.Value.*;

/**
 * @author joe 1/15/2015
 */
@ModuleMeta(name = "ElevatorClaw")
@Log("Claw")
public class BeanClaw implements Claw {
    
    public BeanClaw(DoubleSolenoid leftClawSolenoid, DoubleSolenoid rightClawSolenoid, Solenoid noodleSolenoid){
        this.leftClawSolenoid = leftClawSolenoid;
        this.rightClawSolenoid = rightClawSolenoid;
        this.noodleSolenoid = noodleSolenoid;
    }
    
    DoubleSolenoid leftClawSolenoid;
    DoubleSolenoid rightClawSolenoid;
    Solenoid noodleSolenoid;
    
    FutureTask<Boolean> leftExecute = null;
    FutureTask<Boolean> rightExecute = null;
    FutureTask<Boolean> noodleExecute = null;
    
    @Override
    public void setLeftState(ClawState state) {
        if(state != getLeftState()){
        	if(leftExecute != null){
        		leftExecute.cancel(true);
        	}
            if(state == ClawState.CLOSED){
                leftClawSolenoid.set(Value.kReverse);
            }
            if(state == ClawState.OPEN){
                leftClawSolenoid.set(Value.kForward);
            }
            else
            {
                leftClawSolenoid.set(kOff);
            }
        }
    }

    @Override
    public void setRightState(ClawState state) {
        if(state != getRightState()){
        	if(rightExecute != null){
        		rightExecute.cancel(true);
        	}
            if(state == ClawState.CLOSED){
                rightClawSolenoid.set(Value.kReverse);
            }
            if(state == ClawState.OPEN){
                rightClawSolenoid.set(Value.kForward);
            }
            else
            {
                rightClawSolenoid.set(kOff);
            }
        }
    }

    @Override
    public void setNoodleState(boolean contracted) {
        if(noodleExecute != null){
        	noodleExecute.cancel(true);
        }
    	if (getNoodleState()!=contracted){
            noodleSolenoid.set(contracted);
        }
    }

    @Override
    public ClawState getLeftState() {
        switch(leftClawSolenoid.get().value){
            case kOff_val :
                return ClawState.POWERED_OFF;
            case kForward_val:
                return ClawState.OPEN;
            case kReverse_val:
                return ClawState.CLOSED;
            default:
                return null;
        }
    }

    @Override
    public ClawState getRightState() {
        switch(rightClawSolenoid.get().value){
            case kOff_val :
                return ClawState.POWERED_OFF;
            case kForward_val:
                return ClawState.OPEN;
            case kReverse_val:
                return ClawState.CLOSED;
            default:
                return null;
        }
    }

    @Override
    public boolean getNoodleState() {
        return noodleSolenoid.get();
    }

    @Override
    public void enable() {
        rightClawSolenoid.set(Value.kOff);
        leftClawSolenoid.set(Value.kOff);
        noodleSolenoid.set(false);
    }

    @Override
    public void disable() {
        rightClawSolenoid.set(Value.kOff);
        leftClawSolenoid.set(Value.kOff);
        noodleSolenoid.set(false);
    }
    
    @Override
    public Future<?> script(ScriptElement element) throws ScriptException {
        switch(element.getCommand()){
            case "openLeftClaw":
                if(leftExecute != null){
                    throw new ScriptException("There is already a script manipulating the left claw.");
                }
                leftExecute = new FallbackFutureTask<>(() -> {
                    setLeftState(ClawState.OPEN);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    leftExecute = null;
                }, () -> {
                    leftExecute = null;
                    setLeftState(ClawState.POWERED_OFF);
                },true);
                BeanBot.getI().getAsyncThreadPool().execute(leftExecute);
                return leftExecute;
            case "closeLeftClaw":
            	if(leftExecute != null){
            		throw new ScriptException("There is already a script manipulating the left claw.");
            	}
            	leftExecute = new FallbackFutureTask<>(() -> {
                    setLeftState(ClawState.CLOSED);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e){
                        throw new RuntimeException(e);
                    }
                    leftExecute = null;
                }, () -> {
                    leftExecute = null;
                    setLeftState(ClawState.POWERED_OFF);
                },true);
                BeanBot.getI().getAsyncThreadPool().execute(leftExecute);
                return leftExecute;
            case "openRightClaw":
            	 if(rightExecute != null){
                     throw new ScriptException("There is already a script manipulating the right claw.");
                 }
                 rightExecute = new FallbackFutureTask<>(() -> {
                     setRightState(ClawState.OPEN);
                     try {
                         Thread.sleep(1000);
                     } catch (InterruptedException e) {
                         throw new RuntimeException(e);
                     }
                     rightExecute = null;
                 }, () -> {
                     rightExecute = null;
                     setRightState(ClawState.POWERED_OFF);
                 },true);
                 BeanBot.getI().getAsyncThreadPool().execute(rightExecute);
                 return rightExecute;
            case "closeRightClaw":
            	if(rightExecute != null){
                    throw new ScriptException("There is already a script manipulating the right claw.");
                }
                rightExecute = new FallbackFutureTask<>(() -> {
                    setRightState(ClawState.CLOSED);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    rightExecute = null;
                }, () -> {
                    rightExecute = null;
                    setRightState(ClawState.POWERED_OFF);
                },true);
                BeanBot.getI().getAsyncThreadPool().execute(rightExecute);
                return rightExecute;
            case "defaultLeft":
                setLeftState(ClawState.POWERED_OFF);
                return getImmediateResponse(true);
            case "defaultRight:":
                setRightState(ClawState.POWERED_OFF);
                return getImmediateResponse(true);
            case "openNoodle":
            	if(noodleExecute != null){
                    throw new ScriptException("There is already a script manipulating the noodle claw.");
                }
                noodleExecute = new FallbackFutureTask<>(() -> {
                    setNoodleState(true);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    noodleExecute = null;
                }, () -> noodleExecute = null,true);
                BeanBot.getI().getAsyncThreadPool().execute(noodleExecute);
                return noodleExecute;
            	case "closeNoodle":
            		if(noodleExecute != null){
                        throw new ScriptException("There is already a script manipulating the noodle claw.");
                    }
                    noodleExecute = new FallbackFutureTask<>(() -> {
                        setNoodleState(false);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                        noodleExecute = null;
                    }, () -> noodleExecute = null,true);
                    BeanBot.getI().getAsyncThreadPool().execute(noodleExecute);
                    return noodleExecute;
            default:
                throw new ScriptException("Command doesn't fall under this module");
        }
    }
    
    public FutureTask<Boolean> getImmediateResponse(boolean response){
        FutureTask<Boolean> ret = new FutureTask<Boolean>(() -> {
        },response);
        ret.run();
        return ret;
    }
}
