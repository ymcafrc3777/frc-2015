package io.ibj.lima3777.rr2015;

import edu.wpi.first.wpilibj.*;
import io.ibj.lima3777.rr2015.log.Log;
import io.ibj.lima3777.rr2015.log.LogLevel;
import io.ibj.lima3777.rr2015.log.LogReactor;
import io.ibj.lima3777.rr2015.log.Loggable;
import io.ibj.lima3777.rr2015.log.console.ConsoleOutput;
import io.ibj.lima3777.rr2015.log.telnet.LogServer;
import io.ibj.lima3777.rr2015.loop.UpdatableGroup;
import io.ibj.lima3777.rr2015.loop.UpdateRunner;
import io.ibj.lima3777.rr2015.module.ModuleRegistrar;
import io.ibj.lima3777.rr2015.script.ScriptController;

import java.io.File;
import java.util.concurrent.*;

/**
 * @author joe 1/14/2015
 * Represents the robot, as well as all internal loops, scripts, and submodules. BASICALLY: main class, and BeanBotMain is the bootstrap.
 */
@Log("Robot")
public class BeanBot implements Loggable {
    
    /*
    Statics
     */
    private static BeanBot i;
    
    public static BeanBot getI(){
        return i;
    }
    
    public static void setI(BeanBot inst){
        if(i != null){
            throw new IllegalStateException("BeanBot has already been set!");
        }
        BeanBot.i = inst;
    }
    
    /*
    State information
     */
    
    BotState state = BotState.DISABLED;
    
    public void setState(BotState state){
        if(this.state != state){
            log("Switching state: " + this.state.name() + " -> " + state.name(), LogLevel.INFO);
            moduleRegistrar.updateModuleState(state);
            this.state = state;
        }
    }
    
    public BotState getState(){
        return state;
    }
    
    /*
    Raw Modules
     */
    CANTalon leftPrimaryDriveTalon = new CANTalon(4);
    CANTalon rightPrimaryDriveTalon = new CANTalon(1);
    CANTalon leftMirrorDriveTalon = new CANTalon(2);
    CANTalon rightMirrorDriveTalon = new CANTalon(3);
 
    CANTalon elevatorTalon = new CANTalon(5);
    DigitalInput elevatorZeroLimitSwitch = new DigitalInput(0);
    
    DoubleSolenoid leftClawSolenoid = new DoubleSolenoid(0,1);
    DoubleSolenoid rightClawSolenoid = new DoubleSolenoid(2,3);
    Solenoid tubePincher = new Solenoid(4);
    
    PowerDistributionPanel powerDistributionPanel = new PowerDistributionPanel();
    
    /*
    Submodules
     */
    
    DriveBase driveBase = new BeanDriveBase(leftPrimaryDriveTalon,rightPrimaryDriveTalon,null);
    Elevator elevator = new BeanElevator(elevatorTalon,elevatorZeroLimitSwitch);
    Claw claw = new BeanClaw(rightClawSolenoid,leftClawSolenoid,tubePincher);
    UI ui = new BeanUI();
    
    /*
    Controllers
     */
    ModuleRegistrar moduleRegistrar = new ModuleRegistrar();

    public ModuleRegistrar getModuleRegistrar(){
        return moduleRegistrar;
    }
    
    UpdatableGroup mainThread = new UpdatableGroup("Main Thread");
    
    public UpdatableGroup getMainThread(){
        return mainThread;
    }

    ThreadPoolExecutor asyncThreadPool;

    {
        asyncThreadPool = new ThreadPoolExecutor(1, Integer.MAX_VALUE, Long.MAX_VALUE, TimeUnit.MILLISECONDS, new SynchronousQueue<>(), new ThreadFactory() {
            volatile int threadCount = 1;
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r,"Async Thread "+threadCount);
                threadCount++;
                t.setUncaughtExceptionHandler((thread, gottaCatchEmAll) -> BeanBot.getI().error("Error caught in thread "+ thread.getName(),gottaCatchEmAll));
                return t;
            }
        });
    }

    public ThreadPoolExecutor getAsyncThreadPool(){
        return asyncThreadPool;
    }
    
    UpdateRunner mainThreadRunner = new UpdateRunner(mainThread,10);//10ms

    public LogReactor getLogReactor() {
        return logReactor;
    }

    LogReactor logReactor = new LogReactor();
    
    ScriptController scriptController = new ScriptController();
    
    public ScriptController getScriptController(){
        return scriptController;
    }
    
    long initClockTime;
    
    public long timeSinceStart(){
        return System.currentTimeMillis()-initClockTime;
    }
    
    public void init(){
        try{
            mainThreadRunner.start();

            initClockTime = System.currentTimeMillis();
            log("Running init...", LogLevel.INFO);
            logReactor.register(new ConsoleOutput());
            LogServer socketHandler = new LogServer(1337); //CUZ YA KNOW PRETTY MUCH
            getAsyncThreadPool().execute(socketHandler);
            logReactor.register(socketHandler);
            File scripts = new File("scripts");
            if(!scripts.isDirectory()){
                scripts.mkdirs();
            }
            scriptController.loadAllScripts(scripts);
            leftMirrorDriveTalon.changeControlMode(CANTalon.ControlMode.Follower);
            leftMirrorDriveTalon.set(leftPrimaryDriveTalon.getDeviceID());
            rightMirrorDriveTalon.changeControlMode(CANTalon.ControlMode.Follower);
            rightMirrorDriveTalon.set(rightPrimaryDriveTalon.getDeviceID());
            powerDistributionPanel.clearStickyFaults();
            moduleRegistrar.registerModule(driveBase);
            moduleRegistrar.registerModule(elevator);
            moduleRegistrar.registerModule(claw);
            moduleRegistrar.registerModule(ui);
            log("Finished robot init.", LogLevel.INFO);
        }
        catch(Exception e){
            error(e);
        }
    }
}
