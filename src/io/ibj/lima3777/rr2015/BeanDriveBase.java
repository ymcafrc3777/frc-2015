package io.ibj.lima3777.rr2015;

import edu.wpi.first.wpilibj.CANTalon;
import io.ibj.lima3777.rr2015.driveController.DriveController;
import io.ibj.lima3777.rr2015.driveController.DriveControllerHook;
import io.ibj.lima3777.rr2015.driveController.SimpleScriptedDriveController;
import io.ibj.lima3777.rr2015.log.Log;
import io.ibj.lima3777.rr2015.loop.Updatable;
import io.ibj.lima3777.rr2015.module.ModuleMeta;
import io.ibj.lima3777.rr2015.script.ScriptElement;
import io.ibj.lima3777.rr2015.script.ScriptException;

import java.util.concurrent.Future;

/**
 * @author joe 1/14/2015
 * This drive base uses a 2 level archetecture for determining what controller to use for driving the robot.
 * The driveController is always updated and used until it calls endController() through the DriveControllerHook.
 * That call will cause the fallback to be set as the main controller. In case that fallback is null, it will simply
 * disable the primary drivecontroller, halting the robot.
 */
@ModuleMeta(name="DriveBase")
@Log("DriveBase")
public class BeanDriveBase implements DriveBase, Updatable {
    
    public BeanDriveBase(CANTalon leftTalon, CANTalon rightTalon, DriveController initialDriveController){
        if(leftTalon == null || rightTalon == null){
            throw new NullPointerException();
        }
        reference = new DriveControllerHook(leftTalon, rightTalon, () -> setDriveController(getFallbackController()));
        this.driveController = initialDriveController;
    }

    DriveControllerHook reference = null;
    
    DriveController driveController = null;
    DriveController fallbackController = null;
    
    double x = 0;
    double y = 0;
    double heading = 0;
    
    private static final double ENCODER_CONSTANT = 0.74809175;
    private static final double WHEEL_BASE = 098243098D; //TODO: Actually find this

    @Override
    public DriveController getCurrentController() {
        return driveController;
    }

    @Override
    public void setDriveController(DriveController controller) {
        if(this.driveController != null){
            this.driveController.disable(reference);
        }
        this.driveController = controller;
        if(controller != null) {
            controller.enable(reference);
        }
        else
        {
            reference.getLeftTalon().changeControlMode(CANTalon.ControlMode.Disabled);
            reference.getRightTalon().changeControlMode(CANTalon.ControlMode.Disabled);
        }
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getHeading() {
        return heading;
    }

    @Override
    public void setX(double x) {
        this.x = x;
    }

    @Override
    public void setY(double y) {
        this.y = y;
    }

    @Override
    public void setHeading(double heading) {
        this.heading = heading;
    }

    @Override
    public DriveController getFallbackController() {
        return fallbackController;
    }

    @Override
    public void setFallbackController(DriveController controller) {
        this.fallbackController = controller;
        if(driveController == null){
            setDriveController(fallbackController);
        }
    }

    @Override
    public void enable() {

        reference.getLeftTalon().changeControlMode(CANTalon.ControlMode.PercentVbus);
        reference.getRightTalon().changeControlMode(CANTalon.ControlMode.PercentVbus);
        
        reference.getLeftTalon().enableControl();
        reference.getRightTalon().enableControl();
        
        lastLeftEncoderPosition = reference.getLeftTalon().getEncPosition();
        lastRightEncoderPosition = reference.getRightTalon().getEncPosition();
        
        if(driveController != null){
            driveController.enable(reference);
        }
        
        BeanBot.getI().getMainThread().addUpdatable(this);
    }

    @Override
    public void disable() {
        if(driveController != null){
            driveController.disable(reference);
        }
        reference.getLeftTalon().changeControlMode(CANTalon.ControlMode.Disabled);
        reference.getLeftTalon().disableControl();
        reference.getRightTalon().changeControlMode(CANTalon.ControlMode.Disabled);
        reference.getRightTalon().disableControl();
        reference.getLeftTalon().set(0);
        reference.getRightTalon().set(0);

        BeanBot.getI().getMainThread().removeUpdatable(this);
    }

    int lastLeftEncoderPosition;
    int lastRightEncoderPosition;
    
    @Override
    public void update() {
        double a = (reference.getLeftTalon().getEncPosition()-lastLeftEncoderPosition) * ENCODER_CONSTANT;
        double b = (reference.getRightTalon().getEncPosition()-lastRightEncoderPosition) * ENCODER_CONSTANT;
        double theta = (b-a)/WHEEL_BASE;
        double w;
        if(theta != 0){
            double k = a/theta;
            w = k+WHEEL_BASE/2;
        }
        else
        {
            w = a; //A and B will be the same
        }
        heading += theta;
        x += w*Math.cos(theta);
        y += w*Math.sin(theta);
        if(driveController != null){
            driveController.update(reference);
        }
    }

    @Override
    public Future<?> script(ScriptElement element) throws ScriptException {
        if(element.getCommand().equals("drive")){
            try {
                Double leftPower = Double.valueOf(element.getArguments().get(0));
                Double rightpower = Double.valueOf(element.getArguments().get(1));
                Long time = (long)(Double.valueOf(element.getArguments().get(2))*1000);
                SimpleScriptedDriveController simpleScriptedDriveController = new SimpleScriptedDriveController(this,leftPower,rightpower,time+System.currentTimeMillis());
                setDriveController(simpleScriptedDriveController);
                return simpleScriptedDriveController;
            }
            catch(Exception e){
                throw new ScriptException(e);
            }
            
        }
        else
        {
            throw new ScriptException("No command known by the name of "+element.getCommand());
        }
    }
}
