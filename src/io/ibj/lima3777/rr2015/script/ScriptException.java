package io.ibj.lima3777.rr2015.script;

/**
 * @author joe 1/11/2015
 * Represents any exception or error thrown by a script.
 */
public class ScriptException extends Exception {

    public ScriptException(String message) {
        super(message);
    }

    public ScriptException(Throwable cause) {
        super(cause);
    }
    
}
