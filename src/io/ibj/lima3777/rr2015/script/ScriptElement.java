package io.ibj.lima3777.rr2015.script;

import java.util.ArrayList;
import java.util.List;

/**
 * @author joe 1/11/2015
 * 
 * Represents a single script element. This includes a command (String) and arguments (String[]). 
 */
public class ScriptElement {
    
    private String command;
    private List<String> arguments;
    
    public ScriptElement(String command){
        this.command = command;
        this.arguments = new ArrayList<>();
    }
    
    public ScriptElement(String command, String... arguments){
        this.command = command;
        this.arguments = new ArrayList<>(arguments.length);
        for(String arg : arguments){
            this.arguments.add(arg);
        }
    }
    
    public String getCommand(){
        return command;
    }
    
    public List<String> getArguments(){
        return arguments;
    }

}