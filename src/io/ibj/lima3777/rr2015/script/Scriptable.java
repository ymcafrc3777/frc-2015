package io.ibj.lima3777.rr2015.script;

import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

/**
 * @author joe 1/11/2015
 * 
 * Represents any part of the robot with the ability to be scripted using basic lines of a configuration file.
 * This will be controlled by a seperate controller. Seperate commands may be executed async and together, and it
 * is the module's responsibility to be able to react. 
 */
public interface Scriptable {

    /**
     * Runs a script within the specified module. If the command is not supported by this module, the module should a version of ScriptException.
     * If there is an error with an argument, the script should throw a ScriptException, wrapping the underlying exception.
     */
    Future<?> script(ScriptElement element) throws ScriptException;
    
}
