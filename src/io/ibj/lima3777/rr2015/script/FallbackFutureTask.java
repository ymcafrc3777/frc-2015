package io.ibj.lima3777.rr2015.script;

import java.util.concurrent.FutureTask;

/**
 * @author joe 1/21/2015
 */
public class FallbackFutureTask<T extends Object> extends FutureTask<T> {
    public FallbackFutureTask(Runnable normal, Runnable cancel, T value) {
        super(normal,value);
        this.cancelFallback = cancel;
    }

    Runnable cancelFallback;

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        boolean wasRunning = !(isDone() || isCancelled());
        boolean ret = super.cancel(mayInterruptIfRunning);
        if(wasRunning) {
            try {
                cancelFallback.run();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return ret;
    }
}