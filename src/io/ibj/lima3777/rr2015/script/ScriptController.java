package io.ibj.lima3777.rr2015.script;

import io.ibj.lima3777.rr2015.BeanBot;
import io.ibj.lima3777.rr2015.log.Log;
import io.ibj.lima3777.rr2015.log.Loggable;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;

/**
 * @author joe 1/11/2015
 * 
 * Manages the execution of scripts. 
 */
public class ScriptController implements Loggable {
    
    private Map<String, Scriptable> commandMap = new HashMap<>();
    private Map<String, Script> scriptMap = new HashMap<>();
    
    public void registerScriptable(Scriptable scriptable, String... commands){
        for(String s : commands){
            commandMap.put(s,scriptable);
        }
    }
    
    public Future<?> executeScript(final Script script) throws InterruptedException {
        FutureTask<Boolean> ret = new FutureTask<>(() -> {
            for(Set<ScriptElement> scriptLevel : script){ //For each level to be run in parallel
                Set<Future<?>> scriptResults = new HashSet<>();
                for(ScriptElement element : scriptLevel){
                    try {
                        scriptResults.add(runScriptElement(element));
                    } catch (ScriptException e) {
                        
                    }
                }
                while(!ScriptController.this.isDone(scriptResults)){
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        
                    }
                }
                for(Future<?> future : scriptResults){
                    try{
                        future.get();
                    } catch (ExecutionException | CancellationException | InterruptedException e) {
                        error(e);
                    }
                }
            }
        },true);
        BeanBot.getI().getAsyncThreadPool().execute(ret);
        return ret;
    }
    
    public Future<?> executeScript(String script) throws InterruptedException {
        Script skript = scriptMap.get(script);
        if(skript != null){
            return executeScript(skript);
        }
        else
        {
            throw new RuntimeException("Script not found by the name of "+script+".");
        }
    }
    
    public void registerScript(String string, Script script){
        scriptMap.put(string, script);
    }
    
    private boolean isDone(Set<Future<?>> resultSet){
        boolean allFinished = true;
        for(Future<?> f : resultSet){
            if(!f.isDone()){
                allFinished = false;
                break;
            }
        }
        return allFinished;
    }
    
    public Future<?> runScriptElement(ScriptElement element) throws ScriptException {
        Scriptable found = commandMap.get(element.getCommand());
        if(found == null){
            throw new IllegalStateException("There is no scriptable registered for that command!");
        }
        return found.script(element);
    }
    
    public void loadAllScripts(File baseDirectory){
        loadAllScripts("",baseDirectory);
    }
    
    public void loadAllScripts(String prefix, File baseDirectory){
        File[] files = baseDirectory.listFiles();
        if(files == null){
            return;
        }
        for(File file : files){
            if(file.isDirectory()){
                loadAllScripts(prefix+file.getName()+".",file);
            }
            if(file.getName().endsWith(".script")){
                String name = file.getName().substring(0,file.getName().length()-7);
                try {
                    scriptMap.put(prefix+name,loadScript(file));
                } catch (IOException e) {
                    error(e);
                }
            }
        }
    }
    
    public Script loadScript(File f) throws IOException {
        try(Reader fileReader = new FileReader(f); BufferedReader bufferedReader = new BufferedReader(fileReader)){
            Script ret = new Script();
            Set<ScriptElement> levelElements = new HashSet<>();
            while(bufferedReader.ready()){
                String line = bufferedReader.readLine();
                if(line.equalsIgnoreCase("break")){
                    ret.addLevel(levelElements);
                    levelElements.clear();
                }
                else
                {
                    String[] lineSplit = line.split(" ");
                    if(lineSplit.length  == 1){
                        levelElements.add(new ScriptElement(lineSplit[0]));
                    }
                    else if(lineSplit.length != 0) { // >1 element
                        String[] args = new String[lineSplit.length - 1];
                        System.arraycopy(lineSplit, 1, args, 0, lineSplit.length - 1);
                        levelElements.add(new ScriptElement(lineSplit[0], args));
                    }
                }
            }
            return ret;
        }
    }
}
