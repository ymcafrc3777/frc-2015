package io.ibj.lima3777.rr2015.script;

/**
 * @author joe 1/13/2015
 */
public class ScriptJob implements Runnable {

    public ScriptJob(ScriptController controller, ScriptElement element){
        this.controller = controller;
        this.element = element;
    }
    
    private ScriptController controller;
    
    public ScriptController getController(){
        return controller;
    }
    
    private ScriptElement element;
    
    public ScriptElement getElement(){
        return element;
    }
    
    private boolean finished = false;
    public boolean isFinished(){
        return finished;
    }
    
    @Override
    public void run() {
        try {
            controller.runScriptElement(element);
        } catch (ScriptException e) {
            controller.error(e);
        }
        finished = true;
    }
}
