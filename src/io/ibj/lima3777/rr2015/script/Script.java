package io.ibj.lima3777.rr2015.script;

import java.util.*;

/**
 * @author joe 1/11/2015
 * 
 * Represents an executable script that can be used in autonomous period or as a keybinding. Each script contains a series of steps, each with one or more script calls.
 * 
 * On each level of the script, all script elements will be called in parallel. Once all script elements have completed, the script will move to the next script level, until all
 * script levels have been run. If an error is caught on any script level, the error will be logged, but the script will continue.  
 */
public class Script implements Iterable<Set<ScriptElement>>{
    
    private List<Set<ScriptElement>> scriptLevels = new LinkedList<>();
    
    public Script(){
        
    }
    
    public Script(Set<ScriptElement> firstLevel){
        this.scriptLevels.add(firstLevel);
    }
    
    public Script(ScriptElement... firstElements){
        addLevel(firstElements);
    }
    
    public Script(List<Set<ScriptElement>> elements){
        scriptLevels = elements;
    }
    
    public void addLevel(ScriptElement... elements){
        Set<ScriptElement> firstLevel = new HashSet<>();
        for(ScriptElement element : elements){
            firstLevel.add(element);
        }
        scriptLevels.add(firstLevel);
    }
    
    public void addLevel(Set<ScriptElement> elements){
        scriptLevels.add(elements);
    }
    
    public int getLevels(){
        return scriptLevels.size();
    }
    
    public Set<ScriptElement> getElementLevel(int level){
        return scriptLevels.get(level);
        
    }
    
    @Override
    public Iterator<Set<ScriptElement>> iterator() {
        return scriptLevels.iterator();
    }
}
