package io.ibj.lima3777.rr2015;

import io.ibj.lima3777.rr2015.module.Module;
import io.ibj.lima3777.rr2015.script.Scriptable;

/**
 * @author joe 1/11/2015
 * Represents the pneumatic claw. Can independently manipulate the left, right and noodle apparatus.
 * The left and right claws default to a powered off position, and the noodle state is defaulted to open. 
 */
public interface Claw extends Module, Scriptable {

    /**
     * Sets the left pneumatic claw state.
     * @param state State of the pneumatic claw.
     */
    void setLeftState(ClawState state);

    /**
     * Sets the right pneumatic claw state.
     * @param state State of the pneumatic claw
     */
    void setRightState(ClawState state);

    /**
     * Sets the state of the noodle holding apparatus. 
     * @param contracted Whether the noodle apparatus should be contracted.
     */
    void setNoodleState(boolean contracted);

    /**
     * Returns the state of the left claw. 
     * @return State of the left claw
     */
    ClawState getLeftState();

    /**
     * Returns the state of the right claw. 
     * @return State of the right claw
     */
    ClawState getRightState();

    /**
     * Returns the state of the noodle grabbing apparatus.
     * @return State of the noodle apparatus.
     */
    boolean getNoodleState();
}
