package io.ibj.lima3777.rr2015.driveController;

import edu.wpi.first.wpilibj.CANTalon;
import io.ibj.lima3777.rr2015.DriveBase;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


/**
 * @author joe 1/31/2015
 */
public class SimpleScriptedDriveController implements DriveController, Future<Boolean>
{
    public SimpleScriptedDriveController(DriveBase base, double leftPower, double rightPower, long endTimeMs){
        this.base = base;
        this.leftPower = leftPower;
        this.rightPower = rightPower;
        this.endTimeMs = endTimeMs;
    }
    
    private DriveBase base;
    private double leftPower;
    private double rightPower;
    private long endTimeMs;
    private volatile boolean cancelled;
    
    @Override
    public DriveBase getDriveBase() {
        return base;
    }

    @Override
    public void enable(DriveControllerHook object) {
        object.getLeftTalon().changeControlMode(CANTalon.ControlMode.PercentVbus);
        object.getRightTalon().changeControlMode(CANTalon.ControlMode.PercentVbus);
    }

    @Override
    public void disable(DriveControllerHook object) {
        if(!isDone()){
            cancelled = true;
        }
    }

    @Override
    public void update(DriveControllerHook hook) {
        if(isDone()){
            hook.endController();
            return;
        }
        hook.getRightTalon().set(rightPower);
        hook.getLeftTalon().set(leftPower);
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        cancelled = true;
        return true;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public boolean isDone() {
        return cancelled || endTimeMs <= System.currentTimeMillis();
    }

    @Override
    public Boolean get() throws InterruptedException, ExecutionException {
        if(!isDone()){
            throw new RuntimeException("Operation not done, does not support before finish.");
        }
        return true;
    }

    @Override
    public Boolean get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return get();
    }
}
