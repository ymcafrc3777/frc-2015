package io.ibj.lima3777.rr2015.driveController;

import edu.wpi.first.wpilibj.CANTalon;

/**
 * @author joe 1/31/2015
 */
public class DriveControllerHook {
    
    public DriveControllerHook(CANTalon left, CANTalon right, Runnable endController){
        this.left = left;
        this.right = right;
        this.endController = endController;
    }
    
    Runnable endController;
    
    CANTalon left;
    CANTalon right;
    
    public CANTalon getLeftTalon(){
        return left;
    }
    
    public CANTalon getRightTalon(){
        return right;
    }
    
    public void endController(){
        endController.run();
    }
    
}
