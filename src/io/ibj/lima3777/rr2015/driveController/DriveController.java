package io.ibj.lima3777.rr2015.driveController;

import io.ibj.lima3777.rr2015.DriveBase;
import io.ibj.lima3777.rr2015.module.special.HookModule;
import io.ibj.lima3777.rr2015.loop.special.HookUpdatable;

/**
 * @author joe 1/14/2015
 * Represents a method to control the DriveBase.
 * All controllers represent updatable even if they do not do anything with the method.
 * All controllers are modules, and are enabled and disabled by the DriveBase
 */
public interface DriveController extends HookUpdatable<DriveControllerHook>, HookModule<DriveControllerHook> {
    
    DriveBase getDriveBase();
    
}
