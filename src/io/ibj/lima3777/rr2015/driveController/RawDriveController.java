package io.ibj.lima3777.rr2015.driveController;

import edu.wpi.first.wpilibj.CANTalon;
import io.ibj.lima3777.rr2015.DriveBase;
import io.ibj.lima3777.rr2015.module.ModuleMeta;

/**
 * @author joe 1/14/2015
 * Just injects raw robot power into the robot
 */
@ModuleMeta(name = "Raw Drive Controller")
public class RawDriveController implements DriveController {
    
    public RawDriveController(DriveBase driveBase){
        if(driveBase == null){
            throw new NullPointerException();
        }
        this.driveBase = driveBase;
    }
    
    DriveBase driveBase;
    
    double leftPower = 0D;
    double rightPower = 0D;
    
    public void setLeftPower(double leftPower){
        if(leftPower > 1 || leftPower < -1){
            throw new IllegalArgumentException("Left power must satisfy -1 <= x <= 1");
        }
        this.leftPower = leftPower;
    }
    
    public void setRightPower(double rightPower){
        if(rightPower > 1 || rightPower < -1){
            throw new IllegalArgumentException("Right power must satisfy -1 <= x <= 1");
        }
        this.rightPower = rightPower;
    }
    
    public void set(double leftPower, double rightPower){
        setLeftPower(leftPower);
        setRightPower(rightPower);
    }
    
    @Override
    public DriveBase getDriveBase() {
        return driveBase;
    }

    @Override
    public void update(DriveControllerHook hook) {
        
        hook.getLeftTalon().set(leftPower);
        hook.getRightTalon().set(rightPower);
    }

    @Override
    public void enable(DriveControllerHook object) {
        object.getLeftTalon().changeControlMode(CANTalon.ControlMode.PercentVbus);
        object.getRightTalon().changeControlMode(CANTalon.ControlMode.PercentVbus);
    }

    @Override
    public void disable(DriveControllerHook object) {

    }
}
