package io.ibj.lima3777.rr2015;

import io.ibj.lima3777.rr2015.module.Module;

/**
 * @author joe 1/14/2015
 * Controls all User Interface controls.  
 */
public interface UI extends Module {
    double getLeftY();
    double getRightY();
    
    boolean getLeftClawIn();
    boolean getLeftClawOut();
    
    boolean getRightClawIn();
    boolean getRightClawOut();
    
    boolean getPinchersIn();
    boolean getPinchersOut();
    
    int getElevatorHotkey();
    
    boolean getLeftTrigger();
    boolean getRightTrigger();
}
