package io.ibj.lima3777.rr2015.loop;

import io.ibj.lima3777.rr2015.BeanBot;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author joe
 *         1/10/2015
 */
public class UpdateRunner implements Runnable{
    
    private final long period;
    protected final Updatable updatable;
    
    volatile boolean enable = false;
    
    public UpdateRunner(Updatable updatable, long period){
        if(updatable == null){
            throw new IllegalArgumentException();
        }
        if(period < 0){
            throw new IllegalArgumentException();
        }
        this.updatable = updatable;
        this.period = period;
    }
    
    public void start(){
        enable = true;
        BeanBot.getI().getAsyncThreadPool().execute(this);
    }
    
    public void stop(){
        enable = false;
    }

    @Override
    public void run() {
        while(enable) {
            long startTime = System.currentTimeMillis();
            updatable.update();
            long delayTime = period - (System.currentTimeMillis() - startTime);
            if (delayTime > 0) {
                try {
                    Thread.sleep(delayTime);
                } catch (InterruptedException e) {
                    BeanBot.getI().error(e);
                }
            }
        }
    }
    
    
}
