package io.ibj.lima3777.rr2015.loop;


import io.ibj.lima3777.rr2015.BeanBot;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author joe
 *         1/10/2015
 */
public class UpdatableGroup implements Updatable{

    public UpdatableGroup(String string, Updatable... updatables){
        this.name = string;
        for(Updatable updatable : updatables){
            subUpdatables.add(updatable);
        }
    }
    
    public UpdatableGroup(String name, Collection<Updatable> updatables){
        this.name = name;
        if(updatables == null){
            throw new NullPointerException();
        }
        subUpdatables.addAll(updatables);
    }
    
    String name;
    
    Set<Updatable> subUpdatables = new HashSet<>();

    @Override
    public synchronized void update() {
        for (Updatable updatable : subUpdatables) {
            if (updatable != null) {
                try {
                    updatable.update();
                } catch (Exception gottaCatchEmAll) {
                    BeanBot.getI().error("Error caught within group "+name+"!", gottaCatchEmAll);
                }
            }
        }
    }
    
    public synchronized void addUpdatable(Updatable... updatables){
        for(Updatable updatable : updatables){
            subUpdatables.add(updatable);
        }
    }
    
    public synchronized void removeUpdatable(Updatable... updatables){
        for(Updatable updatable : updatables){
            subUpdatables.remove(updatable);
        }
    }
}
