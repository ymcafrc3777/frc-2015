package io.ibj.lima3777.rr2015.loop;

/**
 * @author joe
 *         1/10/2015
 */
public interface Updatable {
    void update();
}
