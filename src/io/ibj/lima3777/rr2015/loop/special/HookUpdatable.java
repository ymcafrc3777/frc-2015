package io.ibj.lima3777.rr2015.loop.special;

/**
 * @author joe 1/31/2015
 * Represents something that can be updated with a hook reference. This is used in drive controllers to ensure that only one
 * drive controller is controlling at a time.
 */
public interface HookUpdatable<T extends Object> {
    
    void update(T hook);
    
}
