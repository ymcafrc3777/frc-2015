
package io.ibj.lima3777.rr2015.boot;


import edu.wpi.first.wpilibj.SampleRobot;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Timer;
import io.ibj.lima3777.rr2015.BeanBot;
import io.ibj.lima3777.rr2015.BotState;

/**
 * This is a demo program showing the use of the RobotDrive class.
 * The SampleRobot class is the driveBase of a robot application that will automatically call your
 * Autonomous and OperatorControl methods at the right time as controlled by the switches on
 * the driver station or the field controls.
 *
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the SampleRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 *
 * WARNING: While it may look like a good choice to use for your code if you're inexperienced,
 * don't. Unless you know what you are doing, complex code will be much more difficult under
 * this system. Use IterativeRobot or Command-Based instead if you're new.
 */
public final class BeanBotMain extends SampleRobot {
    
    BeanBot bot;

    public BeanBotMain() {
        bot = new BeanBot();
        BeanBot.setI(bot);
    }

    @Override
    protected void robotInit() {
        bot.init();
    }

    @Override
    protected void disabled() {
        bot.setState(BotState.DISABLED);
    }

    /**
     * Drive left & right motors for 2 seconds then stop
     */
    public void autonomous() {
        bot.setState(BotState.AUTO);
    }

    /**
     * Runs the motors with arcade steering.
     */
    public void operatorControl() {
        bot.setState(BotState.TELEOP);
    }

    /**
     * Runs during test mode
     */
    public void test() {
        bot.setState(BotState.TEST);
    }
}
