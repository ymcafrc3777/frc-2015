package io.ibj.lima3777.rr2015;

/**
 * @author joe 1/11/2015
 */
public enum ClawState {
    OPEN,
    CLOSED,
    POWERED_OFF
}
