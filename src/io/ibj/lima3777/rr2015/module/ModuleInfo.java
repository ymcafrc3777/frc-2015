package io.ibj.lima3777.rr2015.module;

import io.ibj.lima3777.rr2015.BotState;
import io.ibj.lima3777.rr2015.log.LogLevel;

import java.util.HashMap;
import java.util.Map;

/**
 * @author joe 1/15/2015
 * In use with and wraps a Module and injects enable information, states of the robot in which the module should be enabled, etc.
 */
public class ModuleInfo {
    
    private static Map<Module, ModuleInfo> moduleMetaMap = new HashMap<>();
    
    public static ModuleInfo getModuleInfo(Module module, ModuleRegistrar moduleRegistrar){
        ModuleInfo meta = moduleMetaMap.get(module);
        if(meta == null){
            meta = new ModuleInfo(module,moduleRegistrar);
            moduleMetaMap.put(module,meta);
        }
        return meta;
    }
    
    private ModuleInfo(Module module, ModuleRegistrar moduleRegistrar){
        this.module = module;
        if(!module.getClass().isAnnotationPresent(ModuleMeta.class)){
            throw new RuntimeException("All modules must have a ModuleMeta tag!");
        }
        ModuleMeta moduleMeta = module.getClass().getAnnotation(ModuleMeta.class);
        enabledBotStates = moduleMeta.enabledStates();
        name = moduleMeta.name();
        dependencies = moduleMeta.dependencies();
        this.moduleRegistrar = moduleRegistrar;
    }
    
    private final Module module;
    private final ModuleRegistrar moduleRegistrar;
    private boolean isEnabled = false;
    private String name;
    private BotState[] enabledBotStates;
    private String[] dependencies;
    
    public String getName(){
        return name;
    }
    
    public void setEnabled(boolean enabled) throws DependenciesUnmetException {
        if(isEnabled == enabled){
            return;
        }
        if(enabled){
            for(String dependencyName : getDependencies()){
                Module dependency = moduleRegistrar.getModule(dependencyName);
                if(dependency == null){
                    throw new DependenciesUnmetException(module,dependencyName);
                }
                ModuleInfo moduleDependency = ModuleInfo.getModuleInfo(dependency, moduleRegistrar);
                if(!moduleDependency.isEnabled){
                    moduleDependency.setEnabled(true);
                    if(!moduleDependency.isEnabled){
                        module.log("Dependency enable exception! Could not enable.",LogLevel.ERROR);
                    }
                }
            }
            try {
                module.enable();
            }
            catch(Exception e){
                module.error(e);
                return;
            }
            module.log("Enabled",LogLevel.INFO);
            isEnabled = true;
        }
        else
        {
            module.log("Disabled",LogLevel.INFO);
            module.disable();
            isEnabled = false;
        }
    }
    
    public boolean shouldBeEnabled(BotState state){
        for(BotState i : enabledBotStates){
            if(i == state){
                return true;
            }
        }
        return false;
    }
    
    public String[] getDependencies(){
        return dependencies;
    }
    
}
