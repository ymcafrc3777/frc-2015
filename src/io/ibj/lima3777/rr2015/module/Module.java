package io.ibj.lima3777.rr2015.module;

import io.ibj.lima3777.rr2015.log.Loggable;

/**
 * @author joe
 *         1/10/2015
 */
public interface Module extends Loggable {
    
    void enable();
    void disable();
    
}
