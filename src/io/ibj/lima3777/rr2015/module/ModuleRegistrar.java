package io.ibj.lima3777.rr2015.module;

import io.ibj.lima3777.rr2015.BeanBot;
import io.ibj.lima3777.rr2015.BotState;
import io.ibj.lima3777.rr2015.log.Log;
import io.ibj.lima3777.rr2015.log.LogLevel;
import io.ibj.lima3777.rr2015.log.Loggable;

import java.util.HashMap;
import java.util.Map;

/**
 * @author joe 1/15/2015
 * Class container designed for registering modules for enable and disable from inputs by BeanBot's state, which is passed to this registrar.
 */
@Log("Module Registrar")
public class ModuleRegistrar implements Loggable{
    
    Map<Class, Module> moduleClassMap = new HashMap<>();
    Map<String, Module> moduleNameMap = new HashMap<>();
    
    public void registerModule(Module module){
        ModuleInfo moduleInfo = ModuleInfo.getModuleInfo(module,this);

        log("Registering module "+moduleInfo.getName(),LogLevel.INFO);
        registerModuleToClass(module,module.getClass());
        moduleNameMap.put(moduleInfo.getName(),module);
        updateModuleState(BeanBot.getI().getState());
    }
    
    private void registerModuleToClass(Module m, Class clazz){
        if(!Module.class.isAssignableFrom(clazz) || Module.class == clazz){
            return;
        }
        if (moduleClassMap.containsKey(clazz)) {
            throw new IllegalArgumentException("There is already an instance of the "+clazz.getName()+" registered!");
        }
        log("Registering class "+clazz.getName()+" to module "+ModuleInfo.getModuleInfo(m,this).getName(), LogLevel.DEBUG);
        moduleClassMap.put(clazz, m);
        Class superclass = clazz.getSuperclass();
        if(superclass != null) {
            registerModuleToClass(m, superclass); //Will never return null, since superclass of all objects is Object
        }
        for(Class clazz2 : clazz.getInterfaces()){
            registerModuleToClass(m,clazz2);
        }
    }
    
    public boolean isRegistered(Module module){
        return moduleClassMap.containsValue(module);
    }
    
    public boolean isRegistered(Class<? extends Module> module){
        return moduleClassMap.containsKey(module);
    }
    
    public <T extends Module> T getModule(Class<T> module){
        return (T) moduleClassMap.get(module);
    }
    public Module getModule(String module){
        return moduleNameMap.get(module);
    }
    
    public void updateModuleState(BotState state){
        log("Updating states to "+state.name(),LogLevel.DEBUG);
        for(Module m : moduleClassMap.values()){
            ModuleInfo info = ModuleInfo.getModuleInfo(m,this);
            try {
                info.setEnabled(info.shouldBeEnabled(state));
            } catch (DependenciesUnmetException e) {
                error(e);
            }
        }
    }
}
