package io.ibj.lima3777.rr2015.module;

/**
 * @author joe 1/21/2015
 */
public class DependenciesUnmetException extends Exception {
    public DependenciesUnmetException(Module myModule, String moduleName) {
        super(myModule+" has unmet dependencies: '"+moduleName+"'");
    }
}
