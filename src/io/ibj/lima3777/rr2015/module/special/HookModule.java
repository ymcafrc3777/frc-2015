package io.ibj.lima3777.rr2015.module.special;

/**
 * @author joe 1/31/2015
 */
public interface HookModule<T extends Object> {
    void enable(T object);
    void disable(T object);
}
