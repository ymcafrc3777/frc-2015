package io.ibj.lima3777.rr2015.module;

import io.ibj.lima3777.rr2015.BotState;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author joe 1/15/2015
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ModuleMeta {
    BotState[] enabledStates() default {BotState.TELEOP,BotState.AUTO,BotState.TEST};
    String[] dependencies() default {};
    String name();
}
