package io.ibj.lima3777.rr2015.log;

/**
 * @author joe 1/26/2015
 */
public enum LogLevel {
    
    ERROR("*",100),
    WARNING("+",10),
    INFO("-",5),
    DEBUG(".",1);
    
    LogLevel(String tag, int level){
        this.level = level;
        this.tag = tag;
    }
    
    String tag;
    int level;
    
    public int getLevel(){
        return level;
    }
    
    public String getTag(){
        return tag;
    }
    
}
