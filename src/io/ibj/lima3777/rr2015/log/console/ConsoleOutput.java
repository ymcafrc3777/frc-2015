package io.ibj.lima3777.rr2015.log.console;

import io.ibj.lima3777.rr2015.log.LogEntry;
import io.ibj.lima3777.rr2015.log.LogOutput;

/**
 * @author joe 1/27/2015
 */
public class ConsoleOutput implements LogOutput {
    @Override
    public void sendLog(LogEntry logEntry) {
        System.out.print(logEntry.getFormattedLine());
    }
}
