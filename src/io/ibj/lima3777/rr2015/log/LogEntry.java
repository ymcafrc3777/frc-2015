package io.ibj.lima3777.rr2015.log;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * @author joe 1/26/2015
 * Represents a log entry 
 */
public class LogEntry {
    
    public LogEntry(String message, Loggable source, String tag, LogLevel level, long msSinceBoot){
        this(message,null,source,tag,level, msSinceBoot);
    }
    
    public LogEntry(String message, Throwable throwable, Loggable source, String tag, LogLevel level, long msSinceBoot){
        this.message = message;
        this.throwable = throwable;
        this.source = source;
        this.tag = tag;
        this.logLevel = level;
        this.msSinceBoot = msSinceBoot;
    }
    
    String message;
    
    public String getMessage(){
        return message;
    }
    
    Throwable throwable;
    
    public Throwable getThrowable(){
        return throwable;
    }
    
    Loggable source;
    
    public Loggable getSource(){
        return source;
    }
    
    String tag;

    public String getTag(){
        return tag;
    }
    
    LogLevel logLevel;
    
    public LogLevel getLevel(){
        return logLevel;
    }
    
    Long msSinceBoot;

    public Long getMsSinceBoot() {
        return msSinceBoot;
    }
    
    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public String getFormattedLine(){
        String exceptionMessage = null;
        if(getThrowable() != null){
            ByteArrayOutputStream stringStream = new ByteArrayOutputStream();
            PrintStream printStream = new PrintStream(stringStream,true);
            getThrowable().printStackTrace(printStream);
            exceptionMessage = new String(stringStream.toByteArray());
        }
        return "["+getMsSinceBoot()/1000D+"]["+getLevel().getTag()+"]["+getTag()+"] "+getMessage() + ((exceptionMessage == null) ? "" : "\r\n"+exceptionMessage)+"\r\n";
    }
    
}
