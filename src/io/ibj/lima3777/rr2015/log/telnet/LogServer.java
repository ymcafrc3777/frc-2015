package io.ibj.lima3777.rr2015.log.telnet;

import io.ibj.lima3777.rr2015.BeanBot;
import io.ibj.lima3777.rr2015.log.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author joe 1/26/2015
 */
@Log("Log Telnet Server")
public class LogServer implements Loggable, Runnable, LogOutput{
    
    public LogServer(int port){
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            error(e);
        }
    }
    
    ServerSocket serverSocket;

    List<LogEntry> entryHistory = new LinkedList<>();
    
    Set<LogServerSocketHandler> socketHandlers = new HashSet<>();

    /*We can run the socket and not worry about a clean close since the only condition of the socket every dying is a
    server/roboRio reboot, so we dont have to really worry about cleaning up our socket, since 1 it will be cleaned up on
    shutdown/restart, and 2 the fact that we will never really get a 'shutdown' signal from the robot... it just powers off...
    FUTURE FEATURE?! #SafeCode */
    @Override
    public void run() {
        while(true){
            try{
                Socket acceptedSocket = serverSocket.accept();//Blocks until connection
                LogServerSocketHandler handler = new LogServerSocketHandler(acceptedSocket,this);
                socketHandlers.add(handler);
                BeanBot.getI().getAsyncThreadPool().execute(handler);
                info("Client accepted at "+acceptedSocket.getInetAddress().toString());
            }
            catch(Exception e){
                error(e);
            }
        }
    }

    @Override
    public void sendLog(LogEntry logEntry) {
        entryHistory.add(logEntry);
        for(LogServerSocketHandler handler : socketHandlers){
            handler.sendLog(logEntry);
        }
    }
}
