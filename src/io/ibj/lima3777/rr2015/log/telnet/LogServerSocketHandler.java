package io.ibj.lima3777.rr2015.log.telnet;

import io.ibj.lima3777.rr2015.log.Log;
import io.ibj.lima3777.rr2015.log.LogEntry;
import io.ibj.lima3777.rr2015.log.LogOutput;
import io.ibj.lima3777.rr2015.log.Loggable;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author joe 1/26/2015
 */
@Log("Log Client")
public class LogServerSocketHandler implements Runnable,LogOutput,Loggable {
    
    public LogServerSocketHandler(Socket socket, LogServer logServer){
        this.socket = socket;
        this.logServer = logServer;
        newLogQueue = new LinkedBlockingQueue<>(logServer.entryHistory);
    }
    
    LogServer logServer;
    Socket socket;
    BlockingQueue<LogEntry> newLogQueue;
    
    @Override
    public void run() {
        info("Started client.");
        while(true){
            try {
                LogEntry newLogEntry = newLogQueue.poll(1000, TimeUnit.MILLISECONDS);//Every second
                if(!socket.isConnected()){
                    //Cleanup
                    logServer.socketHandlers.remove(this);
                    socket.close();
                    return; //End
                }
                if(newLogEntry != null){
                    socket.getOutputStream().write(newLogEntry.getFormattedLine().getBytes());
                    socket.getOutputStream().flush();
                }
            } catch (InterruptedException | IOException e) {
                error(e);
                if(!socket.isClosed()){
                    try {
                        logServer.socketHandlers.remove(this);
                        socket.close();
                    } catch (IOException e1) {
                        error(e1);
                    }
                }
            }
        }
    }

    @Override
    public void sendLog(LogEntry logEntry) {
        try {
            newLogQueue.put(logEntry);
        } catch (InterruptedException e) {
            error(e);
        }
    }
}
