package io.ibj.lima3777.rr2015.log;

/**
 * @author joe 1/26/2015
 * Represents anything that can accept logs. 
 */
public interface LogOutput {
    
    void sendLog(LogEntry logEntry);
    
}
