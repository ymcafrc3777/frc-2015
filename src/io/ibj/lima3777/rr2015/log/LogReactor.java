package io.ibj.lima3777.rr2015.log;

import java.util.HashSet;
import java.util.Set;

/**
 * @author joe 1/27/2015
 * Takes your logs and DEVOURS THEM. Then spits it out onto the dumb telnet server & console. 
 */
public class LogReactor implements LogOutput{

    Set<LogOutput> outputs = new HashSet<>();
    
    @Override
    public void sendLog(LogEntry logEntry) {
        for(LogOutput output : outputs){
            output.sendLog(logEntry);
        }
    }
    
    public void register(LogOutput output){
        outputs.add(output);
    }
    
}
