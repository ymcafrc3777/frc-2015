package io.ibj.lima3777.rr2015.log;

import io.ibj.lima3777.rr2015.BeanBot;

/**
 * @author joe 1/26/2015
 */
public interface Loggable {
    default void log(String message, LogLevel logLevel) {
        log(new LogEntry(message,this,this.getClass().getAnnotation(Log.class).value(),logLevel,BeanBot.getI().timeSinceStart()));
    }
    
    default void info(String message){
        log(message,LogLevel.INFO);
    }
    
    default void warning(String message, Exception e){
        log(new LogEntry(message,e,this,this.getClass().getAnnotation(Log.class).value(),LogLevel.WARNING,BeanBot.getI().timeSinceStart()));
    }
    
    default void debug(String message){
        log(message,LogLevel.DEBUG);
    }
    
    default void error(String message, Throwable e){
        log(new LogEntry(message,e,this,this.getClass().getAnnotation(Log.class).value(),LogLevel.ERROR,BeanBot.getI().timeSinceStart()));
    }

    default void error(Throwable e){
        log(new LogEntry(e.getMessage(),e,this,this.getClass().getAnnotation(Log.class).value(),LogLevel.ERROR,BeanBot.getI().timeSinceStart()));
    }
    
    static void log(LogEntry entry){
        BeanBot.getI().getLogReactor().sendLog(entry);
    }
}
