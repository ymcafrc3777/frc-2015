package io.ibj.lima3777.rr2015;

import edu.wpi.first.wpilibj.CANTalon;
import edu.wpi.first.wpilibj.DigitalInput;
import io.ibj.lima3777.rr2015.log.Log;
import io.ibj.lima3777.rr2015.loop.Updatable;
import io.ibj.lima3777.rr2015.module.ModuleMeta;
import io.ibj.lima3777.rr2015.script.FallbackFutureTask;
import io.ibj.lima3777.rr2015.script.ScriptElement;
import io.ibj.lima3777.rr2015.script.ScriptException;

import java.util.concurrent.Future;

/**
 * @author joe 1/14/2015
 */
@ModuleMeta(name="ElevatorDrive")
@Log("Elevator")
public class BeanElevator implements Elevator, Updatable {
    
    public BeanElevator(CANTalon elevatorMotorController, DigitalInput elevatorZeroButton){ //TODO: Maybe integrate zero limit switch into actual CANTalon? Faster && Safer response.
        if(elevatorMotorController == null || elevatorZeroButton == null){
            throw new NullPointerException();
        }
        this.elevatorMotorController = elevatorMotorController;
        this.elevatorZeroLimit = elevatorZeroButton;
        this.encoderZeroPosition = elevatorMotorController.getEncPosition();
    }
    
    boolean targetMode = false;
    CANTalon elevatorMotorController;
    DigitalInput elevatorZeroLimit;
    Integer encoderZeroPosition;
    Integer totalTicksInElevator = 10000; //TODO: Actually find this
    
    private static final double ELEVATOR_ZERO_SPEED = -0.5D; //TODO: Figure out a good value for this
    
    private Future<?> scriptableTask = null;
    
    private void invalidateScriptable(){
        if(scriptableTask != null){
            scriptableTask.cancel(true);
            scriptableTask = null;
        }
    }
    
    @Override
    public void setElevatorTarget(double target) {
        if(encoderZeroPosition == null){
            throw new IllegalStateException("Elevator has not zero'd! Cannot set a new target.");
        }
        if(!targetMode){
            throw new IllegalStateException("The elevator is currently set for raw mode!");
        }
        if(target <= 0 || target >= 1){
            throw new IllegalArgumentException("Target must satisfy 0<=x<=1");
        }
        
        invalidateScriptable();
        
        double encPosition = totalTicksInElevator * target; //Since totalTicks represents the full range from 0 -> 1, and target is always a fraction of that,
            //Then encPosition will give how many encoder ticks up from zero the elevator should be
        encPosition += encoderZeroPosition;
            //Adjusts for the encoder zero
        elevatorMotorController.set(encPosition); //Actually sets the value to the talon. Since the talon intercepts and reads the encoder, we don't have to mess with it further.
    }

    @Override
    public void resetElevatorEncoder() {
        encoderZeroPosition = elevatorMotorController.getEncPosition(); //Zeros out the encoder locally. The CANTalon does not have a local zero feature.
    }

    @Override
    public double getElevatorTarget() {
        if (!targetMode) {
            throw new IllegalStateException("The elevator is currently set for raw mode!");
        } else {
            return (elevatorMotorController.getSetpoint()- encoderZeroPosition)/totalTicksInElevator;//Converts back to 0->1 scale
        }
    }

    @Override
    public double getElevatorPosition() {
        return (elevatorMotorController.getEncPosition()- encoderZeroPosition)/totalTicksInElevator; //Converts back to 0->1 scale
    }

    @Override
    public boolean isTargetEnabled() {
        return targetMode;
    }

    @Override
    public void enableTargeting(boolean enable) {
        if(this.targetMode != enable) {
            invalidateScriptable();
            this.targetMode = enable;
            if (enable) {
                elevatorMotorController.setFeedbackDevice(CANTalon.FeedbackDevice.QuadEncoder);
                elevatorMotorController.changeControlMode(CANTalon.ControlMode.Position);
            } else
                elevatorMotorController.changeControlMode(CANTalon.ControlMode.PercentVbus);
        }
    }

    @Override
    public void setRawMotor(double value) {
        if(targetMode){
            throw new IllegalStateException("The elevator is currently set for targeted mode!");
        }
        if(value <= -1 || value >= 1){
            throw new IllegalArgumentException("Target must satisfy -1<=x<=1");
        }
        invalidateScriptable();
        elevatorMotorController.set(value);
    }

    @Override
    public double getRawMotor() {
        return elevatorMotorController.get(); //TODO: Sometimes returns a sensor value?!
    }

    @Override
    public void forceReZero() {
        if(elevatorZeroLimit.get()){ //If is pressed
            encoderZeroPosition = elevatorMotorController.getEncPosition(); //Resets 
        }
        else {
            elevatorZeroLimit = null; //Will force all checks to throw IllegalStateException. 
            enableTargeting(false); //Sets to raw mode, so we can force the elevator down.
            setRawMotor(ELEVATOR_ZERO_SPEED); //Sends the motor down at elevator speed
        }
    }

    @Override
    public boolean isZeroed() {
        return encoderZeroPosition != null;
    }

    @Override
    public void enable() {
        elevatorMotorController.clearStickyFaults();
        elevatorMotorController.enableControl();
        
        BeanBot.getI().getMainThread().addUpdatable(this);
    }

    @Override
    public void disable() {
        elevatorMotorController.disableControl();
        elevatorMotorController.disable();
        
        BeanBot.getI().getMainThread().removeUpdatable(this);
    }


    @Override
    public void update() {
        if(elevatorZeroLimit.get()) { //Is pressed
            if(getRawMotor() < 0) {
                enableTargeting(false);
                setRawMotor(0);
            }
            encoderZeroPosition = elevatorMotorController.getEncPosition(); //Zeroed!
        }
        //Make sure to zero out elevator first, if not already zero'd out!
        if(encoderZeroPosition == null){
            if(isTargetEnabled()){
                enableTargeting(false); //Force into raw mode
            }
            setRawMotor(ELEVATOR_ZERO_SPEED);
        }
        else
        {
            if(getElevatorPosition()>=1){
                if(getRawMotor() > 0) {
                    enableTargeting(false);
                    setRawMotor(0);
                }
            }
        }
    }

    @Override
    public Future<?> script(ScriptElement element) throws ScriptException {
        switch (element.getCommand()) {
            case "elevatorRaw":
                try {
                    Double power = Double.valueOf(element.getArguments().get(0));
                    if (power < -1 || power > 1) {
                        throw new ScriptException("Power is out of range: Must be between -1 & 1");
                    }
                    Double timePeriod = Double.valueOf(element.getArguments().get(1));
                    if (timePeriod < 0) {
                        throw new ScriptException("NEGATIVE TIME?! TIME TRAVEL IS POSSIBLE!");
                    }
                    enableTargeting(false);
                    setRawMotor(power);
                    return scriptableTask = BeanBot.getI().getAsyncThreadPool().submit(new FallbackFutureTask<>(() -> {
                        try {
                            Thread.sleep((long) (timePeriod / 1000));
                            scriptableTask = null;
                        } catch (InterruptedException e) {
                            scriptableTask = null;
                            throw new RuntimeException(e);
                        }
                    }, () -> elevatorMotorController.set(0), true));
                } catch (Exception e) {
                    throw new ScriptException(e);
                }
            case "elevatorSet":
                try {
                    Double setpoint = Double.valueOf(element.getArguments().get(0));
                    if (setpoint > 1 || setpoint < 0) {
                        throw new ScriptException("Setpoint is out of range! Must be between 0 && 1");
                    }
                    enableTargeting(true);
                    setElevatorTarget(setpoint);
                    return scriptableTask = BeanBot.getI().getAsyncThreadPool().submit(() -> {
                        while (!isOnTarget()) {
                            try {
                                Thread.sleep(10);
                                scriptableTask = null;
                            } catch (InterruptedException e) {
                                scriptableTask = null;
                                throw new RuntimeException(e);
                            }
                        }
                    }, true);
                } catch (Exception e) {
                    throw new ScriptException(e);
                }
            default:
                throw new ScriptException("Command not handled by this module: " + element.getCommand());
        }
    }
    
    public boolean isOnTarget(){
    	if(!targetMode){
    		throw new RuntimeException("The elevator is currently set for targeted mode!");
    	}

    	return Math.abs(getElevatorTarget()-getElevatorPosition()) < .01;
    }
}
