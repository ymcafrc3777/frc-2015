package io.ibj.lima3777.rr2015;

import edu.wpi.first.wpilibj.Joystick;
import io.ibj.lima3777.rr2015.driveController.RawDriveController;
import io.ibj.lima3777.rr2015.log.Log;
import io.ibj.lima3777.rr2015.loop.Updatable;
import io.ibj.lima3777.rr2015.module.ModuleMeta;

/**
 * @author joe 1/15/2015
 */
@ModuleMeta(name="UI",enabledStates = {BotState.TELEOP}, dependencies = {"DriveBase","ElevatorDrive","ElevatorClaw"})
@Log("UI")
public class BeanUI implements UI, Updatable {
    
    Joystick leftJoystick = new BeanJoystick(0);
    Joystick rightJoystick = new BeanJoystick(1);
    
    /*
    Initialized during enable(); Cannot be immediatly initialized through ModuleRegistrar since they may not have been registered yet.
     */
    DriveBase driveBase;
    Elevator elevator;
    Claw claw;
    
    RawDriveController rawDriveController;
    
    static final double ELEVATOR_ADJUST_MULTIPLIER = 1;

    @Override
    public double getLeftY() {
        return leftJoystick.getY();
    }

    @Override
    public double getRightY() {
        return rightJoystick.getY();
    }

    @Override
    public boolean getLeftClawIn() {
        return leftJoystick.getRawButton(5);
    }

    @Override
    public boolean getLeftClawOut() {
        return leftJoystick.getRawButton(4);
    }

    @Override
    public boolean getRightClawIn() {
        return rightJoystick.getRawButton(4);
    }

    @Override
    public boolean getRightClawOut() {
        return rightJoystick.getRawButton(5);
    }

    @Override
    public boolean getPinchersIn() {
        return leftJoystick.getRawButton(3) || rightJoystick.getRawButton(3);
    }

    @Override
    public boolean getPinchersOut() {
        return leftJoystick.getRawButton(2) || rightJoystick.getRawButton(2);
    }

    @Override //TODO: General hotkeys
    public int getElevatorHotkey() {
        for(int i = 6; i<=11; i++){
            if(leftJoystick.getRawButton(i) || rightJoystick.getRawButton(i)){
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean getLeftTrigger() {
        return leftJoystick.getTrigger();
    }

    @Override
    public boolean getRightTrigger() {
        return rightJoystick.getTrigger();
    }

    @Override
    public void update() {
        if(BeanBot.getI().getState() != BotState.TELEOP){
            return; //Do not care.
        }
        if(getLeftTrigger() || getRightTrigger()){
            double elevatorAdjust;
            if(getLeftTrigger() && getRightTrigger()){
                elevatorAdjust = (getRightY() + getLeftY())/2;
            }
            else if(getLeftTrigger()){
                elevatorAdjust = getLeftY();
            }
            else
            {
                elevatorAdjust = getRightY();
            }
            elevatorAdjust *= ELEVATOR_ADJUST_MULTIPLIER;
            elevator.enableTargeting(false);
            elevator.setRawMotor(elevatorAdjust);
        }
        else
        {
            if(!elevator.isTargetEnabled()){
                elevator.setRawMotor(0); //Should probably turn of the motor if it is not being used as targeting for something else.
            }
            //Instead of going to the elevator, the Y's should be used for driving the robot.
            rawDriveController.set(getLeftY(),getRightY());
        }
        
        if(getLeftClawIn()){
            claw.setLeftState(ClawState.CLOSED);
        }
        else if(getLeftClawOut()){
            claw.setLeftState(ClawState.OPEN);
        }
        else
        {
            claw.setLeftState(ClawState.POWERED_OFF);
        }
        
        if(getRightClawIn()){
            claw.setRightState(ClawState.CLOSED);
        }
        else if(getRightClawOut()){
            claw.setRightState(ClawState.OPEN);
        }
        else{
            claw.setRightState(ClawState.POWERED_OFF);
        }
        
        if(getPinchersIn()){
            claw.setNoodleState(true);
        }
        else if(getPinchersOut()){
            claw.setNoodleState(false);
        }
    }

    @Override
    public void enable() {
        driveBase = BeanBot.getI().getModuleRegistrar().getModule(DriveBase.class);
        elevator = BeanBot.getI().getModuleRegistrar().getModule(Elevator.class);
        claw = BeanBot.getI().getModuleRegistrar().getModule(Claw.class);
        
        if(rawDriveController == null) {
            rawDriveController = new RawDriveController(driveBase);
        }
        
        driveBase.setFallbackController(rawDriveController);
        
        BeanBot.getI().getMainThread().addUpdatable(this);
    }

    @Override
    public void disable() {
        BeanBot.getI().getMainThread().removeUpdatable(this);
    }
}
